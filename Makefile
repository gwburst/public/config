
ifndef DATA
CFG_DATA_DIR = GWOSC
else
CFG_DATA_DIR = $(DATA)
endif

all:
	if [ -d $(CFG_DATA_DIR) ]; then						\
	   cd $(CFG_DATA_DIR); make;						\
	   echo "";								\
	   echo "cWB Config DATA Directory is: $(CFG_DATA_DIR)";		\
	   echo "";								\
	else									\
	   echo "";								\
	   echo "cWB Config DATA Directory: $(CFG_DATA_DIR) not exist";		\
	   echo "";								\
	fi;

clean:
	if [ -d $(CFG_DATA_DIR) ]; then						\
	   cd $(CFG_DATA_DIR); make clean;					\
	   echo "";								\
	   echo "Removed cWB Config DATA Link: $(CFG_DATA_DIR)";		\
	   echo "";								\
	else									\
	   echo "";								\
	   echo "cWB Config DATA Directory: $(CFG_DATA_DIR) not exist";		\
	   echo "";								\
	fi;

