#!/bin/bash -f

export O2_GPS_START="1164556817"

export O2_GPS_STOP="1187740818"

export O2_CHUNK_START=2
export O2_CHUNK_STOP=22

export O2_CWB_CONFIG_C02c_CHNAME="GWOSC-4KHZ_R1_STRAIN"
export O2_CWB_CONFIG_C02c_V_CHNAME="GWOSC-4KHZ_R1_STRAIN"


export O2_SN_PROD_CONDOR_TAG=""
export O2_SN_SIM_CONDOR_TAG=""

export O2_BURSTLF_PROD_CONDOR_TAG=""
export O2_BURSTLF_SIM_CONDOR_TAG=""

export O2_BURSTHF_PROD_CONDOR_TAG=""
export O2_BURSTHF_SIM_CONDOR_TAG=""

export O2_BURSTLD_PROD_CONDOR_TAG=""
export O2_BURSTLD_SIM_CONDOR_TAG=""

export O2_BBH_PROD_CONDOR_TAG=""
export O2_BBH_SIM_CONDOR_TAG=""

export O2_IMBHB_PROD_CONDOR_TAG=""
export O2_IMBHB_SIM_CONDOR_TAG=""

