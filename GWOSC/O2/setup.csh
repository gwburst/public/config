#!/bin/tcsh -f

setenv	O2_GPS_START		"1164556817"  	#Wed Nov 30 16:00:00 GMT 2016

setenv	O2_GPS_STOP		"1187740818"   	#Sat Aug 26 00:00:00 GMT 2017

setenv  O2_CHUNK_START          2
setenv  O2_CHUNK_STOP           22

setenv  O2_CWB_CONFIG_C02c_CHNAME       "GWOSC-4KHZ_R1_STRAIN"
setenv  O2_CWB_CONFIG_C02c_V_CHNAME 	"GWOSC-4KHZ_R1_STRAIN"


setenv  O2_SN_PROD_CONDOR_TAG           ""
setenv  O2_SN_SIM_CONDOR_TAG            ""

setenv  O2_BURSTLF_PROD_CONDOR_TAG      ""
setenv  O2_BURSTLF_SIM_CONDOR_TAG       ""

setenv  O2_BURSTHF_PROD_CONDOR_TAG      ""
setenv  O2_BURSTHF_SIM_CONDOR_TAG       ""

setenv  O2_BURSTLD_PROD_CONDOR_TAG      ""
setenv  O2_BURSTLD_SIM_CONDOR_TAG       ""

setenv  O2_BBH_PROD_CONDOR_TAG          ""
setenv  O2_BBH_SIM_CONDOR_TAG           ""

setenv  O2_IMBHB_PROD_CONDOR_TAG        ""
setenv  O2_IMBHB_SIM_CONDOR_TAG         ""

