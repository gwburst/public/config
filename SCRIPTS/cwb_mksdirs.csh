#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_mksdirs
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_lschunk --idir="..."'
  echo ""
  echo "idir     : directory where to create the search directories"
  echo ""
  echo "Ex: cwb_mksdirs --idir /home/user_name/O2"
  echo ""
  exit 1
endif

setenv CWB_MKSDIRS_IDIR          ""

set temp=(`getopt -s tcsh -o d: --long idir: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --idir:
                setenv CWB_MKSDIRS_IDIR          $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_MKSDIRS_IDIR == '' ))) then
  echo ""
  echo "Error: empty input working directory"
  echo "type cwb_mksdirs to list the available options"
  echo ""
  exit 1
endif

mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BBH/LH/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BBH/LH/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstHF/LH/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstHF/LH/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLD/LH/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLF/LH/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLF/LH/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/IMBHB/LH/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/IMBHB/LH/SIM

mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BBH/LHV/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BBH/LHV/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstHF/LHV/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstHF/LHV/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLD/LHV/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLF/LHV/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/BurstLF/LHV/SIM
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/IMBHB/LHV/BKG
mkdir -p $CWB_MKSDIRS_IDIR/SEARCHES/OFFLINE/IMBHB/LHV/SIM

unsetenv CWB_MKSDIRS_IDIR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
