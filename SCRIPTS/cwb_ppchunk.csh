#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_ppchunk
  exit
endif

if ($1 == '') then
  echo ""
  echo 'cwb_ppchunk --run="..." --chunk="..." --cal="..." --net="..." --search="..." --type="..." --tag="..." --mlabel="..." --opt="..."'
  echo ""
  echo "run     : O1, O2"
  echo "chunk   : integer number"
  echo "cal     : C00, C00c, C01, C02, C02c"
  echo "net     : LH LHV"
  echo "search  : BurstLF, BurstHF, BurstLD, IMBHB, BBH"
  echo "type    : BKG, SIM"
  echo "tag     : user string (Ex: run1, tst1, dev1)"
  echo 'mlabel  : merge label, default="M1"'
  echo ""
  echo "options : optional"
  echo "          no options -> report (default)"
  echo "          merge : execute merge"
  echo "          veto  : execute veto"
  echo "          cut   : execute cut"
  echo "          ifar  : execute ifar"
  echo "          report: execute report"
  echo "          all   : execute merge + veto + cut + report"
  echo ""
  echo 'Ex: cwb_ppchunk --run="O2" --chunk="03" --cal="C00" --net="LH" --search="BurstLF" --type="BKG" --tag="dev1" --mlabel="M1"'
  echo "or"
  echo 'Ex: cwb_ppchunk --run=O2 --chunk=03 --cal=C00 --net=LH --search=BurstLF --type=BKG --tag=dev1 --mlabel=M1'
  echo "or"
  echo 'Ex: cwb_ppchunk --run O2 --chunk 03 --cal C00 --net LH --search BurstLF --type BKG --tag dev1 --mlabel M1'
  echo ""
  exit 1
endif

setenv CWB_PPCHUNK_OPTIONS	""
setenv CWB_PPCHUNK_RUN		""
setenv CWB_PPCHUNK_CHUNK	""
setenv CWB_PPCHUNK_CAL		""
setenv CWB_PPCHUNK_NET		""
setenv CWB_PPCHUNK_SEARCH	""
setenv CWB_PPCHUNK_TYPE		""
setenv CWB_PPCHUNK_TAG		""
setenv CWB_PPCHUNK_MLABEL	"M1"
setenv CWB_PPCHUNK_OPTIONS	"all"

set cmd_line="$0 $argv"

set temp=(`getopt -s tcsh -o r:c:C:n:s:t:T:m:o: --long run:,chunk:,cal:,net:,search:,type:,tag:,mlabel:,opt: -- $argv:q`)
if ($? != 0) then
  echo "Terminating..." >/dev/stderr
  exit 1
endif
eval set argv=\($temp:q\)

while (1)
        switch($1:q)
        case -r:
        case --run:
                setenv CWB_PPCHUNK_RUN          $2:q
                shift ; shift
                breaksw
        case -c:
        case --chunk:
                setenv CWB_PPCHUNK_CHUNK        $2:q
                shift ; shift
                breaksw
        case -C:
        case --cal:
                setenv CWB_PPCHUNK_CAL          $2:q
                shift ; shift
                breaksw
        case -n:
        case --net:
                setenv CWB_PPCHUNK_NET          $2:q
                shift ; shift
                breaksw
        case -s:
        case --search:
                setenv CWB_PPCHUNK_SEARCH       $2:q
                shift ; shift
                breaksw
        case -t:
        case --type:
                setenv CWB_PPCHUNK_TYPE         $2:q
                shift ; shift
                breaksw
        case -T:
        case --tag:
                setenv CWB_PPCHUNK_TAG          $2:q
                shift ; shift
                breaksw
        case -m:
        case --mlabel:
                setenv CWB_PPCHUNK_MLABEL       $2:q
                shift ; shift
                breaksw
        case -o:
        case --opt:
                setenv CWB_PPCHUNK_OPTIONS      $2:q
                shift ; shift
                breaksw
        case --:
                shift
                break
        default:
                echo "error - missing parameters!" ; exit 1
        endsw
end

if ((( $CWB_PPCHUNK_RUN != 'O1' ) && ( $CWB_PPCHUNK_RUN != 'O2' ))) then
  echo ""
  echo --run=\'$CWB_PPCHUNK_RUN\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ( $CWB_PPCHUNK_CHUNK !~ ^[0-9]+$ ) then
  echo ""
  echo --chunk=\'$CWB_PPCHUNK_CHUNK\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
else
  if ( $CWB_PPCHUNK_CHUNK == 99 ) then
    if ( $CWB_PPCHUNK_RUN == 'O1' ) then
      @ CHUNK_START = $O1_CHUNK_START
      @ CHUNK_STOP  = $O1_CHUNK_STOP
    endif
    if ( $CWB_PPCHUNK_RUN == 'O2' ) then
      @ CHUNK_START = $O2_CHUNK_START
      @ CHUNK_STOP  = $O2_CHUNK_STOP
    endif
  else
    @ CHUNK_START = $CWB_PPCHUNK_CHUNK
    @ CHUNK_STOP  = $CWB_PPCHUNK_CHUNK
  endif
endif 

if ((( $CWB_PPCHUNK_CAL != 'C00' ) && ( $CWB_PPCHUNK_CAL != 'C00c' ) && ( $CWB_PPCHUNK_CAL != 'C01' ) && ( $CWB_PPCHUNK_CAL != 'C02' ) && ( $CWB_PPCHUNK_CAL != 'C02c' ))) then
  echo ""
  echo --cal=\'$CWB_PPCHUNK_CAL\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_PPCHUNK_NET != 'LH' ) && ( $CWB_PPCHUNK_NET != 'LHV' ))) then
  echo ""
  echo --net=\'$CWB_PPCHUNK_NET\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_PPCHUNK_SEARCH != 'BurstLF' ) && ( $CWB_PPCHUNK_SEARCH != 'BurstHF' ) && ( $CWB_PPCHUNK_SEARCH != 'BurstLD' ) && ( $CWB_PPCHUNK_SEARCH != 'IMBHB' ) && ( $CWB_PPCHUNK_SEARCH != 'BBH' ))) then
  echo ""
  echo --search=\'$CWB_PPCHUNK_SEARCH\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_PPCHUNK_TYPE == 'BKG' ) || ( $CWB_PPCHUNK_TYPE =~ 'SIM/*' ))) then
  setenv CWB_PPCHUNK_TYPE $CWB_PPCHUNK_TYPE
  set CWB_PPCHUNK_SIM=$CWB_PPCHUNK_TYPE
  set CWB_PPCHUNK_SIM_DIR=`echo $CWB_PPCHUNK_SIM | awk '{print substr($0, 5, length($0)-1)}'`
else
  echo ""
  echo --type=\'$CWB_PPCHUNK_TYPE\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_PPCHUNK_TAG == '' ))) then
  echo ""
  echo --tag=\'$CWB_PPCHUNK_TAG\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if ((( $CWB_PPCHUNK_MLABEL == 'M0' ))) then
  echo ""
  echo --mlabel=\'$CWB_PPCHUNK_MLABEL\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if (( $CWB_PPCHUNK_OPTIONS != 'merge' ) && ( $CWB_PPCHUNK_OPTIONS != 'veto' ) && ( $CWB_PPCHUNK_OPTIONS != 'cut' ) && ( $CWB_PPCHUNK_OPTIONS != 'ifar' ) && ( $CWB_PPCHUNK_OPTIONS != 'pe' ) && ( $CWB_PPCHUNK_OPTIONS != 'report' ) && ( $CWB_PPCHUNK_OPTIONS != 'all' )) then
  echo ""
  echo --opt=\'$CWB_PPCHUNK_OPTIONS\' "is a wrong cwb_ppchunk option\n"
  echo "type cwb_ppchunk to list the available options"
  echo ""
  exit 1
endif

if (( $CWB_PPCHUNK_TYPE =~ 'SIM/*' )) then
  setenv CWB_PPCHUNK_DIR $CWB_PPCHUNK_RUN\_K$CWB_PPCHUNK_CHUNK\_$CWB_PPCHUNK_CAL\_$CWB_PPCHUNK_NET\_$CWB_PPCHUNK_SEARCH\_SIM\_$CWB_PPCHUNK_SIM_DIR\_$CWB_PPCHUNK_TAG
else
  setenv CWB_PPCHUNK_DIR $CWB_PPCHUNK_RUN\_K$CWB_PPCHUNK_CHUNK\_$CWB_PPCHUNK_CAL\_$CWB_PPCHUNK_NET\_$CWB_PPCHUNK_SEARCH\_$CWB_PPCHUNK_TYPE\_$CWB_PPCHUNK_TAG
endif


cd $CWB_PPCHUNK_DIR
$CWB_CONFIG/$CWB_PPCHUNK_RUN/SEARCHES/OFFLINE/$CWB_PPCHUNK_SEARCH/$CWB_PPCHUNK_NET/$CWB_PPCHUNK_TYPE/mkpp.csh $CWB_PPCHUNK_MLABEL $CHUNK_START $CHUNK_STOP $CWB_PPCHUNK_OPTIONS $CWB_PPCHUNK_SEARCH
if ( $? != 0) exit 1
# create cWB_config.log file
make -f $CWB_CONFIG/Makefile.log CMD_LINE="$cmd_line" git >& /dev/null

cd ..


unsetenv CWB_PPCHUNK_RUN
unsetenv CWB_PPCHUNK_CHUNK
unsetenv CWB_PPCHUNK_CAL
unsetenv CWB_PPCHUNK_NET
unsetenv CWB_PPCHUNK_SEARCH
unsetenv CWB_PPCHUNK_TYPE
unsetenv CWB_PPCHUNK_TAG
unsetenv CWB_PPCHUNK_MLABEL
unsetenv CWB_PPCHUNK_OPTIONS
unsetenv CWB_PPCHUNK_DIR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
