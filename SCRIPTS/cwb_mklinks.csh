#!/bin/tcsh -f

onintr irq_ctrlc

if ( $1 == '' ) then
  $CWB_SCRIPTS/cwb_help.csh cwb_mklinks
  exit
endif

set cmd_line="$0 $argv"

if ((($1 == '') || ("$2" == '')) && ($1 != 'list')) then
#  $CWB_SCRIPTS/cwb_help.csh cwb_mklinks
  echo ''
  echo "cwb_mklinks M2 '--ilabel M1'"
  echo "cwb_mklinks M2 '--ilabel M1.V_hvetoLH.C_bin1_hvetoLH'"
  echo "cwb_mklinks M2 '--ilabel M1.V_hvetoLH.C_bin1_hvetoLH'" O2_K02_C00_LH_BBH_BKG_run1
  echo ''
  exit
endif

unsetenv CWB_MERGE_LABEL
unsetenv CWB_MKLINKS_OPTIONS
setenv   CWB_MKLINKS_IDIR    "" 

if ($1 == 'list') then
  root -n -l -b ${CWB_PARMS_FILES} ${CWB_MACROS}/cwb_dump_merge_dir.C
  exit
endif

if ( "$2" != '' ) then
  setenv CWB_MKLINKS_OPTIONS "$2"
else
  unsetenv CWB_MKLINKS_OPTIONS
endif

if ($3 != '') then
  setenv CWB_MKLINKS_IDIR "$3"
endif

setenv CWB_MERGE_LABEL $1

if ($CWB_MKLINKS_IDIR != '') cd $CWB_MKLINKS_IDIR

root -n -l -b ${CWB_PARMS_FILES} ${CWB_CONFIG}/MACROS/cwb_mklinks.C
if ( $? != 0) exit 1

# create cWB_analysis.log file
make -f $CWB_SCRIPTS/Makefile.log CMD_LINE="$cmd_line" svn >& /dev/null

if ($CWB_MKLINKS_IDIR != '') cd ..

unsetenv CWB_MERGE_LABEL
unsetenv CWB_MKLINKS_OPTIONS
unsetenv CWB_MKLINKS_IDIR

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

