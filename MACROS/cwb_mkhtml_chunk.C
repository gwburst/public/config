/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "ReadChunkList.C"

#define CHUNK_FILE_LIST         "Chunk_List.txt"
#define CHUNK_MAX_SIZE          100

#define MAX_CED_ENTRIES		10
#define MIN_CED_RHO		4.5
//#define MIN_CED_RHO		0.0

#define WWW_PUBLIC              "https://ldas-jobs.ligo.caltech.edu/~waveburst/reports/"
#define WWW_LAG_MANUAL          "https://gwburst.gitlab.io/documentation/latest/html/faq.html?highlight=lag#what-are-lags-and-how-to-use-them"
#define WWW_SLAG_MANUAL         "https://gwburst.gitlab.io/documentation/latest/html/faq.html?highlight=lag#what-are-super-lags-and-how-to-use-them"


void GetChunkRange(TString run, int ichunk, double& xstart, double& xstop);
void ComputeSigmaFAP(double OBSERVATIONAL_TIME, double BACKGROUND_TIME, int TRIALS_FACTOR=1);
void ComputeSigmaFAP(double& fap, double& sigma, double ifar, double obs_time);

void cwb_mkhtml_chunk(TString run, TString search, int ibin, int ichunk, bool bbh, TString dir_bkg, TString dir_frg, TString odir, TString wlabel, TString net_file_name, int lag, int slag, int nIFO) {

  cout<<"cwb_mkhtml_chunk.C starts..."<<endl;
  //cout << odir << endl;exit(0);

  int rho_id;
  if(search=="BBH" || search=="IMBHB")  rho_id=1;
  else                                  rho_id=0;

  // get CWB_USER_URL
  char cwb_user_url[1024] = WWW_PUBLIC;
  if(gSystem->Getenv("CWB_USER_URL")!=NULL) {
    strcpy(cwb_user_url,TString(gSystem->Getenv("CWB_USER_URL")).Data());
  }

  char schunk[512];sprintf(schunk,"%02d",ichunk);
  char search_title[512];
  if(search=="BBH" || search=="IMBHB") {
    sprintf(search_title,"%s Search",search.Data());
  } else {
    sprintf(search_title,"%s Search Bin%d",search.Data(),ibin);
  }

  double xstart=0; 
  double xstop=0;
  GetChunkRange(run, ichunk, xstart, xstop);
  cout << xstart << " " << xstop << endl;

  double interval = (xstop-xstart)/(24.*3600.);

  wat::Time beg_date(xstart);
  wat::Time end_date(xstop);

  TString sbeg_date = beg_date.GetDateString();sbeg_date.Resize(19);
  TString send_date = end_date.GetDateString();send_date.Resize(19);

  char period[1024];
  sprintf(period,"GPS Interval [%d,%d]. UTC Interval %s - %s. Interval duration = %.2f days.",int(xstart),int(xstop),sbeg_date.Data(),send_date.Data(),interval);

  char box_title[1024];
  if(lag==0 && slag==0) 
    sprintf(box_title,"Open Box Result");
  else
    sprintf(box_title,"Fake Open Box Result - ( <td><a href=\"%s\" target=\"_blank\">LAG</a></td> = %d - <td><a href=\"%s\" target=\"_blank\">SLAG</a></td> = %d )",WWW_LAG_MANUAL,lag,WWW_SLAG_MANUAL,slag);
//    sprintf(box_title,"Fake Open Box Result - ( LAG = %d - SLAG = %d )",lag,slag);

  char bkg_rep[1024];
  sprintf(bkg_rep,"<td><a href=\"%s/%s/%s/index.html\" target=\"_blank\">Background Report</a></td>",cwb_user_url,wlabel.Data(),dir_bkg.Data());

  // get livetime   

  char fileliv[1024];
  sprintf(fileliv,"../../%s/data/live.txt",dir_bkg.Data());
  cout << fileliv << endl;
  int countlag=0;
  double OLIVETIME = GetLiveTime(fileliv,slag,lag,countlag);   	// get zero  livetime
  double LIVETIME  = GetLiveTime(fileliv,-1,-1,countlag); 	// get total livetime
  LIVETIME-=OLIVETIME;  // non zero live time
  countlag-=1;          // subtract the zero lag 
  cout.precision(14);
  cout << OLIVETIME << " " << LIVETIME << endl;
  //ComputeSigmaFAP(OLIVETIME, LIVETIME);

  char livetime[1024];
  sprintf(livetime,"Livetime -   Background: %.2f years, Foreground: %.2f days",LIVETIME/(24.*3600.*365.),OLIVETIME/(24.*3600.));

  // create body.html file

  ofstream out;
  char fileout[1024];
  sprintf(fileout,"%s/body.html", odir.Data());
  cout << fileout << endl;
  out.open(fileout,ios::out);
  if (!out.good()) {cout << "Error Opening File : " << fileout << endl;exit(1);}

  out << "<html>" << endl;

//  out << "<br>" << endl;
  out << "<div align=\"center\"><font color=\"blue\"><h1>" << search_title << " : Chunk " << schunk << "</h1></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"red\"><h4>"<< box_title <<"</h4></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"black\"><h4>"<< period <<"</h4></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"black\"><h4>"<< livetime <<"</h4></font></div>" << endl;
  out << "<div align=\"center\"><font color=\"black\"><h4>"<< bkg_rep <<"</h4></font></div>" << endl;
//  out << "<br>" << endl;

  out << "<hr>" << endl;
  out << "<br>" << endl;
  out << "<br>" << endl;

  out << "<table>" << endl;
  out << "<tr><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>FAR vs Rank</h2></font>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"FARvsRank\">" << endl;
  out << "<img src=\"FARvsRank.png\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>Cumulative Number vs IFAR</h2></font>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"CumulativeNumberVsIFAR_bbh_plot\">" << endl;
  if(bbh) out << "<img src=\"CumulativeNumberVsIFAR_bbh_plot.png\" width=\"470\"> </a>" << endl;
  else    out << "<img src=\"CumulativeNumberVsIFAR_nobbh_plot.png\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td></tr>" << endl;
  out << "</table>" << endl;

  out << "<table>" << endl;
  out << "<tr><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>Background: Rank vs Time</h2></font>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"rho_time\">" << endl;
  out << "<img src=\"rho_time.gif\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td><td width=\"50%\"><div align=\"center\">" << endl;
  out << "<font color=\"red\"><h2>Background: Rank vs Frequency</h2></font>" << endl;
  out << "</div><div align=\"center\"><ul><br/>" << endl;
  out << "<a class=\"image\" title=\"rho_frequency\">" << endl;
  out << "<img src=\"rho_frequency.gif\" width=\"470\"> </a>" << endl;
  out << "</br></ul>" << endl;
  out << "</div><br><br></td></tr>" << endl;
  out << "</table>" << endl;


  TChain wave("waveburst");
  wave.Add(net_file_name);
  netevent  W(&wave,nIFO);

  gSystem->Exec("date");

  char cut[1024];
  sprintf(cut,"lag[%d]==%d && slag[%d]==%d",nIFO,lag,nIFO,slag);

  char sel[1024];
  sprintf(sel,"rho[%d]:Entry$",rho_id);

  W.fChain->SetEstimate(W.fChain->GetEntries());
  W.fChain->Draw(sel,cut,"goff");
  Int_t nentries = (Int_t)W.fChain->GetSelectedRows();
  Int_t *index = new Int_t[nentries];
  double* entry = W.fChain->GetV2();
  TMath::Sort(nentries,W.fChain->GetV1(),index,true);

  char os[1024];

  int pp_inetcc = 0;

  char ifo[3][8] = {"L1","H1","V1"};

  out << "<head>" << endl;
  out << "<style type=\"text/css\">" << endl;
  out << ".datagrid tr:hover td" << endl;
  out << "{" << endl;
  out << "        background-color:#F1F1F2;" << endl;
  out << "}" << endl;
  out << "</style>" << endl;
  out << "</head>" << endl;
  out << "<b>" << endl;
  out << "<hr>" << endl;
  out << "<br>" << endl;
  out << "<font color=\"red\" style=\"font-weight:bold;\"><center><p><h2>Foreground Loudest Event List</h2><p><center></font>" << endl;
  if(bbh) out << "<a target=\"_blank\" href=\"CumulativeNumberVsIFAR_bbh_loudest.txt\">(Events: Cumulative FAP) </a>" << endl;
  else    out << "<a target=\"_blank\" href=\"CumulativeNumberVsIFAR_nobbh_loudest.txt\">(Events: Cumulative FAP) </a>" << endl;
  out << "<br>" << endl;
  out << "<br>" << endl;
  out << "</html>" << endl;

  out << "<table border=0 cellpadding=2 class=\"datagrid\">" << endl;
  out << "<tr align=\"center\">"<< endl;
  out << "<td>CED</td>"<< endl;
  out << "<td>rho</td>"<< endl;
  out << "<td>IFAR(yr)</td>"<< endl;
//  out << "<td>FAP</td>"<< endl;
//  out << "<td>lag</td>"<< endl;
//  out << "<td>slag</td>"<< endl;
  out << "<td>SNRnet</td>"<< endl;
//  out << "<td>Correlation["<<pp_inetcc<<"]</td>"<< endl;
  out << "<td>Correlation</td>"<< endl;
  out << "<td>Frequency(Hz)</td>"<< endl;
  out << "<td>Bandwidth(Hz)</td>"<< endl;
  out << "<td>Duration(sec)</td>"<< endl;
//  out << "<td>run</td>"<< endl;
  for (int nn=0;nn<nIFO;nn++) out << "<td>GPS " << ifo[nn] << "</td>"<< endl;
  for (int nn=0;nn<nIFO;nn++) out << "<td>SNR " << ifo[nn] << "</td>"<< endl;
  out << "</tr>"<< endl;

  out << "<td></td>" << endl;

  int segEdge=10;

  float ifar;
  W.fChain->SetBranchAddress("ifar",&ifar);

  int ecount=0;
  for(int i=0; i<nentries; i++) {

    if(i>=MAX_CED_ENTRIES) continue;

    W.fChain->GetEntry(entry[index[i]]);

    if(W.rho[rho_id]<=MIN_CED_RHO) continue;

    double fap;
    double sigma;
    ComputeSigmaFAP(fap, sigma, ifar, OLIVETIME);

    cout.precision(2);
    cout << "EVENT " << i <<  "\trho = " << W.rho[rho_id] << "\tnetcc = " << W.netcc[0] << "\tifar (years) = " << ifar/(24.*3600.*365.) 
         << "\tobs (days) = " << OLIVETIME/(24.*3600.) << "\tfap = " << fap << "\tsigma = " << sigma << endl;
    ecount++;
    //cout << ecount << " ";cout.flush();
    out << "<tr align=\"center\">"<< endl;
    sprintf(os,"run==%i",W.run);
//    TString s_ced=GetFileLabel(W.fChain,W.run,W.lag[nIFO],W.slag[nIFO],segEdge,wlabel);

    char label[1024];
    int start = TMath::Nint(W.start[0]-W.left[0])+segEdge;
    int stop  = TMath::Nint(W.stop[0]+W.right[0])-TMath::Nint(W.start[0]-W.left[0])-2*segEdge;
    sprintf(label,"%i_%i_%s_slag%i_lag%i_%i_job%i", start, stop, wlabel.Data(), (int)W.slag[nIFO], (int)W.lag[nIFO], 1, W.run);
    TString s_ced=label;

    char namedir[1024];
    sprintf(namedir,"");
    for (int nn=0; nn<nIFO; nn++) sprintf(namedir,"%s%s",namedir,ifo[nn]);
    for (int nn=0; nn<nIFO; nn++) {
      sprintf(namedir,"%s_%.3f",namedir,W.start[nn]);
    }
    sprintf(os,"<td><a href=\"%s/%s/%s/ced/ced_%s/%s\" target=\"_blank\">%i</a></td>",cwb_user_url,wlabel.Data(),dir_frg.Data(),s_ced.Data(),namedir,ecount);

//    cout << os << endl;
    out << os << endl;
    sprintf(os,"<td>%.2f</td>",W.rho[rho_id]);
    out << os << endl;
    sprintf(os,"<td>%.2g</td>",ifar/(24.*3600.*365.));
    out << os << endl;
//    sprintf(os,"<td>%.2g</td>",fap);
//    out << os << endl;
//    sprintf(os,"<td>%i</td>",(int)W.lag[nIFO]);
//    out << os << endl;
//    sprintf(os,"<td>%i</td>",(int)W.slag[nIFO]);
//    out << os << endl;
    sprintf(os,"<td>%3.1f</td>",sqrt(W.likelihood));
    out << os << endl;
    sprintf(os,"<td>%.2f</td>",W.netcc[pp_inetcc]);
    out << os << endl;
    sprintf(os,"<td>%i</td>",(int)W.frequency[1]);
    out << os << endl;
    sprintf(os,"<td>%i</td>",(int)W.bandwidth[1]);
    out << os << endl;
    sprintf(os,"<td>%.3f</td>",W.duration[1]);
    out << os << endl;
    for (int nn=0; nn<nIFO; nn++) {
      sprintf(os,"<td>%.2f</td>",W.time[nn]);
      out << os << endl;
    }
    for (int nn=0; nn<nIFO; nn++) {
      sprintf(os,"<td>%3.1f</td>",sqrt(W.sSNR[nn]));
      out << os << endl;
    }

    out << "</tr>" << endl;

  } // End event loop

  out << "</table>" << endl;
  out << "<p>" << endl;
  out << endl;

  exit(0);
}

void GetChunkRange(TString run, int ichunk, double& xstart, double& xstop) {

  // get CWB_CONFIG
  char cwb_config_env[1024] = "";
  if(gSystem->Getenv("CWB_CONFIG")!=NULL) {
    strcpy(cwb_config_env,TString(gSystem->Getenv("CWB_CONFIG")).Data());
  }

  char chunk_file_list[1024];
  sprintf(chunk_file_list,"%s/%s/CHUNKS/%s",cwb_config_env,run.Data(),CHUNK_FILE_LIST);
  cout << chunk_file_list << endl;

  int    chunk[CHUNK_MAX_SIZE];
  double start[CHUNK_MAX_SIZE];
  double stop[CHUNK_MAX_SIZE];

  int nChunks = ReadChunkList(chunk_file_list,chunk,start,stop);

  for(int i=0;i<nChunks;i++) {
    if(chunk[i]==ichunk) {
      xstart = start[i];
      xstop  = stop[i];
    }
  }
}

void ComputeSigmaFAP(double OBSERVATIONAL_TIME, double BACKGROUND_TIME, int TRIALS_FACTOR) {

  double N = OBSERVATIONAL_TIME*(1./BACKGROUND_TIME);
  double FAP = 1-exp(-N*TRIALS_FACTOR);
  double Gsigma = sqrt(2)*TMath::ErfcInverse(FAP);

  double FAP_from_Gsigma = TMath::Erfc(Gsigma*1./sqrt(2));    // xcheck

  cout << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "OBSERVATIONAL_TIME : " << OBSERVATIONAL_TIME/(24.*3600.) << " days" << endl;
  cout << "BACKGROUND_TIME    : " << BACKGROUND_TIME/(24.*3600.*365.) << " years" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "FAP                : " << FAP << endl;
  cout << "Gaussian sigma     : " << Gsigma << endl;
  cout << "-----------------------------------------------" << endl;
  cout << endl;

}

void ComputeSigmaFAP(double& fap, double& sigma, double ifar, double obs_time) {

  double N = obs_time/ifar;
  double FAP = 1-exp(-N);
  double Gsigma = sqrt(2)*TMath::ErfcInverse(FAP);

  double FAP_from_Gsigma = TMath::Erfc(Gsigma*1./sqrt(2))/2;    // xcheck

/*
  cout << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "OBSERVATIONAL_TIME : " << obs_time/(24.*3600.) << " days" << endl;
  cout << "BACKGROUND_TIME    : " << ifar/(24.*3600.*365.) << " years" << endl;
  cout << "-----------------------------------------------" << endl;
  cout << "FAP                : " << FAP << endl;
  cout << "Gaussian sigma     : " << Gsigma << endl;
  cout << "-----------------------------------------------" << endl;
  cout << endl;
*/

  fap = FAP;
  sigma = Gsigma;
}

