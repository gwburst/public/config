# ROOT Macro 

The following macros are used to create chunk files and to merge hveto, are executed with make under CHUNK and VETOES directories 

* cwb_lschunk.C (used to dump availables chunks: cWB-config/O*/CHUNKS/Chunk_List.txt)
* cwb_mklinks.C (used by command cwb_mklinks to make the symbolic links of merged production files)
* ReadChunkList.C (used to read chunk list file: cWB-config/O*/CHUNKS/Chunk_List.txt)
* CreateChunkFiles.C (used to create chunk files: cWB-config/O*/CHUNKS/K*.period)
* CreateChunkCuts.C  (used to create pp chunk file: cWB-config/O*/CHUNKS/Chunks_Cuts.hh)
* CreateMergedHVETO.C (used to merge hveto files in the directory cWB-config/O*/DATA/VETOES/)
* MergeSegments.C (used by CreateMergedHVETO.C)	
* cwb_mkhtml_chunk.C (used to create html pages for single chunks results)
* cwb_mkhtml_all.C (used to create html page for multiple chunk open/close box report)
* Make_PP_IFAR.C (used to produce the observed vs IFAR plots)

