/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


// creates new merge label using symbolic links  : used by the cwb_mklinks command

{

  CWB::Toolbox TB;

  int estat;
  Long_t id,size,flags,mt;
  char cmd[1024];

  TString cwb_merge_label     = TString(gSystem->Getenv("CWB_MERGE_LABEL"));
  TString cwb_mklinks_options = TString(gSystem->Getenv("CWB_MKLINKS_OPTIONS"));

  // check if label has the correct format (M#) & extract merge number
  int cwb_merge_number=0;
  if(cwb_merge_label[0]!='M') {
    cout << "Error : label " << cwb_merge_label.Data() << " has bad format (M#)" << endl;exit(1);
  } else {
    TString lcheck=cwb_merge_label;
    lcheck.Remove(0,1);
    if(!lcheck.IsDigit()) {
      cout << endl << "Error : label " << cwb_merge_label.Data() << " has bad format (M#)" << endl << endl;exit(1);
    }
    cwb_merge_number=lcheck.Atoi();
  }

  // get file list with data_label tag
  char tag[256];sprintf(tag,"wave_%s",data_label);
  vector<TString> fileList = TB.getFileListFromDir(merge_dir, ".root", tag,"",true);

  // compute new version for label
  int iversion=0;
  for(int i=0;i<fileList.size();i++) {
    //cout << i << " " << fileList[i].Data() << endl;
    if(fileList[i].Contains(".root")) {
      TObjArray* token = TString(fileList[i]).Tokenize(TString("."));
      TString srescueID = ((TObjString*)token->At(token->GetEntries()-2))->GetString();
      srescueID.ReplaceAll("M","");
      if(srescueID.IsDigit()) {
        //cout << i << " " << fileList[i].Data() << endl;
        int rescueID = srescueID.Atoi();
        if(iversion<rescueID) iversion=rescueID;
      }
    }
  }
  iversion++;

  if(cwb_merge_number==0 || cwb_merge_number>=iversion) {       // iversion replaced with the input user merge version
     if(cwb_merge_number!=0) iversion=cwb_merge_number;
  } else {
     cout << endl << "cwb_merge.C : Error - the input merge version (M" << cwb_merge_number
          << ") must be greater of the most recent merge version (M" << iversion-1 << ")" << endl << endl;
     gSystem->Exit(1);
  }

  TString mdir = merge_dir;

  // check if cwb_mklinks_options contains '--' than cwb_mklinks_options
  // is used to extract all setcuts parameters
  TString cwb_ilabel = "";
  if(cwb_mklinks_options.Contains("--")) {
    TString option="";
    // get the cwb_tcuts_tree
    cwb_ilabel = TB.getParameter(cwb_mklinks_options,"--ilabel");
  }

  // check if cwb_ilabel is defined
  if(cwb_ilabel=="") {
    cout << "cwb_mklikns.C : Error - ilabel not defined" << endl;
    gSystem->Exit(1);
  }
 
  char tmp[256]=""; 
  char fTag[256]=""; 
  TString cwb_imerge_label="";
  wavearray<int> fIndex(fileList.size()); fIndex=0;
  TObjArray* token = TString(cwb_ilabel).Tokenize(TString("."));
  for(int i=0;i<token->GetEntries();i++) {
    TString stoken = ((TObjString*)token->At(i))->GetString();
    // get input merge label
    if(i==0) cwb_imerge_label=stoken;
    //cout << "TOKEN : " << i << " " << stoken << endl;
    // check if file exist
    bool exist=false;
    sprintf(tmp,"%s",fTag);
    sprintf(fTag,"%s.%s",tmp,stoken.Data());
    char fName[1024];sprintf(fName,"%s/wave_%s%s.root",mdir.Data(),data_label,fTag);
    for(int i=0;i<fileList.size();i++) {
      //cout << i << " " << fileList[i].Data() << " " << fName << endl;
      if(fileList[i]==fName) {exist=true;fIndex[i]=1;break;}
    }
    if(!exist) {
      cout << endl << "cwb_mklikns.C : Error - file: " << fName << " not exist!!!" << endl << endl;
      gSystem->Exit(1);
    }
  }

/*
  cout << endl;
  for(int i=0;i<fileList.size();i++) {
    if(fIndex[i]) cout << i << " " << fileList[i].Data() << endl;
  }
  cout << endl;
*/

  cout << endl;
  cout << "merge dir        : " << mdir << endl;
  cout << "cwb_ilabel       : " << cwb_ilabel << endl;
  cout << "cwb_imerge_label : " << cwb_imerge_label << endl;
  cout << "cwb_merge_label  : " << cwb_merge_label << endl;
  cout << endl;

  for(int i=0;i<fileList.size();i++) {
    if(fIndex[i]) {
      //cout << i << " " << fileList[i].Data() << endl;

      TString iwfname = fileList[i];
      iwfname.ReplaceAll(mdir+"/","");
      TString owfname = iwfname;
      owfname.ReplaceAll(cwb_imerge_label,cwb_merge_label);

      TString ilfname = iwfname;
      ilfname.ReplaceAll("wave_","merge_");
      ilfname.ReplaceAll(".root",".lst");
      TString olfname = owfname;
      olfname.ReplaceAll("wave_","merge_");
      olfname.ReplaceAll(".root",".lst");
  
      TString imfname = iwfname;
      if(simulation) imfname.ReplaceAll("wave_","mdc_"); else imfname.ReplaceAll("wave_","live_");
      TString omfname = owfname;
      if(simulation) omfname.ReplaceAll("wave_","mdc_"); else omfname.ReplaceAll("wave_","live_");

      //cout << endl;
      //cout << "iwfname: " << iwfname << endl;
      //cout << "owfname: " << owfname << endl;
      //cout << "imfname: " << imfname << endl;
      //cout << "omfname: " << omfname << endl;
      //cout << endl;


      estat = gSystem->GetPathInfo(mdir+"/"+iwfname,&id,&size,&flags,&mt);
      if(estat==0) {
        sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),iwfname.Data(),owfname.Data());
        cout << cmd << endl;
        gSystem->Exec(cmd);
      }

      estat = gSystem->GetPathInfo(mdir+"/"+ilfname,&id,&size,&flags,&mt);
      if(estat==0) {
        sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),ilfname.Data(),olfname.Data());
        cout << cmd << endl;
        gSystem->Exec(cmd);
      }

      estat = gSystem->GetPathInfo(mdir+"/"+imfname,&id,&size,&flags,&mt);
      if(estat==0) {
        sprintf(cmd,"cd %s;ln -sf %s %s",mdir.Data(),imfname.Data(),omfname.Data());
        cout << cmd << endl;
        gSystem->Exec(cmd);
      }
    }
  }

  exit(0);
}
