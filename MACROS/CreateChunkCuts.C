/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include "ReadChunkList.C"

#define CHUNK_FILE_LIST         "Chunk_List.txt"
#define CHUNK_MAX_SIZE          100

#define OFILE_CHUNK_CUTS	"Chunks_Cuts.hh"

//#define EXECUTE

void CreateChunkCuts(TString run) {

  // get CWB_CONFIG
  char cwb_config_env[1024] = "";
  if(gSystem->Getenv("CWB_CONFIG")!=NULL) {
    strcpy(cwb_config_env,TString(gSystem->Getenv("CWB_CONFIG")).Data());
  }

  char chunk_file_list[1024];
  sprintf(chunk_file_list,"%s/%s/CHUNKS/%s",cwb_config_env,run.Data(),CHUNK_FILE_LIST);
  cout << chunk_file_list << endl;

  int    chunk[CHUNK_MAX_SIZE];
  double start[CHUNK_MAX_SIZE];
  double stop[CHUNK_MAX_SIZE];

  int nChunks = ReadChunkList(chunk_file_list,chunk,start,stop);

  char tcut[256];

  char ofile[1024];
  sprintf(ofile,"%s/%s/CHUNKS/%s",cwb_config_env,run.Data(),OFILE_CHUNK_CUTS);
  cout << "output file list : " << ofile << endl;
#ifdef EXECUTE
  ofstream out;
  out.open(ofile,ios::out);
#endif
  for(int k=0;k<nChunks;k++) {
//    cout << "\t" << chunk[k] << "\t" << (int)start[k] << "\t" << (int)stop[k] << endl;

    sprintf(tcut,"TCut O2_K%02d_cut(\"O2_K%02d_cut\",\"time[0]>%d && time[0]<=%d\");",chunk[k],chunk[k],(int)start[k],(int)stop[k]);
    
#ifdef EXECUTE
    out << tcut << endl;
#else
    cout << tcut << endl;
#endif

  }

  // create cut chunk99 : full period
  sprintf(tcut,"TCut O2_K%02d_cut(\"O2_K%02d_cut\",\"time[0]>%d && time[0]<=%d\");",99,99,(int)start[0],(int)stop[nChunks-1]);
#ifdef EXECUTE
  out << tcut << endl;
#else
  cout << tcut << endl;
#endif


#ifdef EXECUTE
  out.close();
#endif

  exit(0);

} 
