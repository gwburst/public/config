#define RUN_LABEL "BBH_SEOBNR_M10_150_F1_12 O1 C02"
//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
#define FAR_BIN1_FILE_NAME "report/postprod/M3.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt"

{
  #include "O1/SEARCHES/OFFLINE/BBH/LH/PP_Cuts.hh"
  #include "O1/CHUNKS/Chunks_Cuts.hh"

  calibVer      = "#CWB_CALIB_VER";
 
  user_pp_label = "";
  bool INCLUDE_INTERNAL_VOLUME = 0;
  #include "/home/waveburst/O1/SEARCHES/CBC/BBH/PP_Cuts.hh"

  TString far_bin1_cut_file[100];

  char xfile[1024];
  for(int n=1;n<=9;n++) {
    sprintf(xfile,"/home/vedovato/O1/REPROCESSING/BBH/O1_K%02d_C02_LH_BBH_BKG_run1/%s", n, FAR_BIN1_FILE_NAME); //WARNING: placeholder!
    far_bin1_cut_file[n] = xfile;
  }

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O1/DATA/%s/HVETO/L1/HVETO_L1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O1/DATA/%s/HVETO/H1/HVETO_H1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());

}
