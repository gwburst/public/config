// THIS IS THE NEW VERSION OF CONFIG PLUGIN !!!
// THE NEW VERSION IS REQUIRED BY ROOT6
// IN ROOT5 IS STILL POSSIBLE TO USE OLD & NEW VERSION

#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  // Config Plugin to generate injected 'on the fly' NR signals

  cout << "Execute CWB_PluginConfig.C ..." << endl;

  network** net;
  CWB_PLUGIN_IMPORT(network**,net);

  int* gIFACTOR;
  CWB_PLUGIN_IMPORT(int*,gIFACTOR);

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);


//All numbers, files etc. related to the the MonteCarlo....
  int SEGS[9][3] =
{
  {1, 1126051217, 1127271617},
  {2, 1127271617, 1128299417},
  {3, 1128299417, 1129383017},
  {4, 1129383017, 1130754617},
  {5, 1130754617, 1132104617},
  {6, 1132104617, 1133173817},
  {7, 1133173817, 1134450017},
  {8, 1134450017, 1135652417},
  {9, 1135652417, 1137258496}
};

  int* xstart;
  int* xstop;

  bool check = false;
  CWB_PLUGIN_CHECK(int*,xstop,check);

  if(check==true) {
    CWB_PLUGIN_IMPORT(int*,xstart);
    CWB_PLUGIN_IMPORT(int*,xstop);
  } else {
    xstart = new int;
    xstop  = new int;
    *xstart = SEGS[0][1];
    *xstop  = SEGS[1][2];
  }
//  int* xstart;
 // int* xstop;
 // CWB_PLUGIN_IMPORT(int*,xstart);
 // CWB_PLUGIN_IMPORT(int*,xstop);
 // int xstart[1];
 // int xstop[1];
 // xstart[0] = 1126052217;
 // cout << "Start: " << xstart[0] << endl;
 // xstop[0]  = 1126053217; 
  //cout << "Stop: " << xstop[0] << endl;
  
  int k = 0;
 // bool included = 0;
  while((*xstart>SEGS[k][1]) && (k<9)) {
        //cout << "k=" << k << endl;
	if(*xstop<SEGS[k][2]){cout << "Segment is fully included in chunk " << SEGS[k][0] << " " << SEGS[k][1] << " " << SEGS[k][2] << endl; break;}
        else {
	      if (*xstart<SEGS[k][2]){cout << "Segment is not fully included in chunk " << SEGS[k][0] << " " << SEGS[k][1] << " " << SEGS[k][2] << endl;}	
              else{k++;}
        }
	//cout << "k=" << k << endl;	    
  }

  char st[1024];
  //sprintf(st,"-}, %s/O2/SEARCHES/OFFLINE/BBH/LH/SIM/BBH_BROAD_ISOTROPIC/XML/bbh_broad_isotropic},", cwb_config_env);
  TString IDIR = "/work/salemi/git/lvc-rates-and-pop/share/O2/injection_files"; 
  sprintf(st,"--xml %s/O1_%d/bbh_broad_isotropic-%d-%d.xml", IDIR.Data(), SEGS[k][0], SEGS[k][1], SEGS[k][2]);
  //sprintf(st,"--xml bbh_broad_isotropic-%d-%d.xml", SEGS[k][1], SEGS[k][2]);
  cout << "Opening XML file: " << st << endl;
  TString inspOptions="";
  inspOptions+=st;
  inspOptions+= " --dir "+TString((*cfg)->tmp_dir)+" ";
  // the first parameter a the name of MDC defined by the user
  MDC->SetInspiral("SEOBNRv3_opt_rk4pseudoFourPN",inspOptions);
 
}

