#!/bin/tcsh -f

onintr irq_ctrlc

if ($1 == '') then
  echo ""
  echo "Error : missing input parameter !!!"
  echo ""
  exit
endif

cwb_clonedir ../BurstLF/O1_K$1_C02_LH_BurstLF_BKG_run1 O1_K$1_C02_LH_IMBHB_BKG_run1 '--output merge'

cd O1_K$1_C02_LH_IMBHB_BKG_run1
cp /home/waveburst/O1/SEARCHES/CBC/IMBHB/user_pparameters.C config/user_pparameters.C

cwb_merge M1 
cwb_setveto M1

cwb_setcuts M1.V_hvetoLH "--tcuts bin1_cut --label bin1_cut"
cwb_report  M1.V_hvetoLH.C_bin1_cut create 

cwb_setifar M1.V_hvetoLH.C_bin1_cut '--xtsel run>=0  --label ifar --xfile report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt --mode exclusive'

cd ..

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1
