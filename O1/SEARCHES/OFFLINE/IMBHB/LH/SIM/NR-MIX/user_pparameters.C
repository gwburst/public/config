
#define nRHO 300

// -----------------------------------------------------------
// CBC
// -----------------------------------------------------------

#define RUN_LABEL "O1 IMBH redshifted NR 19 MDC sets: C02 Callbration"

#define MIN_plot_mass1 0.0
#define MAX_plot_mass1 450
#define MIN_plot_mass2 0.0
#define MAX_plot_mass2 450

#define MAX_EFFECTIVE_RADIUS  5000.
#define MASS_BIN 30.0
#define MIN_MASS 0.0
#define MAX_MASS 900.

#define MINCHI -1.0
#define MAXCHI 1.0
#define CHI_BIN 0.15

#define TOLERANCE 0.01

#define BKG_NTUPLE "True"
#define PLOT_CHIRP
#define PLOT_MASS_RATIO
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
#define WRITE_ASCII
#define LIVE_ZERO 3978522.0

#define PLOT_CHIRP
#define PLOT_MASS_RATIO
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
#define FIXMAXDISTANCE 6800000
#define FIXMINDISTANCE 0

#define FIXMINRATIO 0.95
#define FIXMAXRATIO 13.05
#define FIXMINTOT MIN_plot_mass1+MIN_plot_mass2
#define FIXMAXTOT MAX_plot_mass1+MAX_plot_mass2
#define WRITE_ASCII
#define REDSHIFT

#define POINTMASSES

//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
{

  #include <O1/SEARCHES/OFFLINE/IMBHB/LH/PP_Cuts.hh>
  #include <O1/CHUNKS/Chunks_Cuts.hh>

  #define nCHUNKS	9
  #define NFACTOR	19
  // ---------------------------------------------------------------
  // All numbers, files etc. related to the the MonteCarlo....
  // ---------------------------------------------------------------
 
  user_pp_label = "";
  //INCLUDE_INTERNAL_VOLUME = 0;
  //TRIALS = 1;

  int NINJ[NFACTOR];
  NINJ[0]=112083;
  NINJ[1]=112051;
  NINJ[2]=112041;
  NINJ[3]=112127;
  NINJ[4]=112027;
  NINJ[5]=112047;
  NINJ[6]=112058;
  NINJ[7]=112045;
  NINJ[8]=112108;
  NINJ[9]=112131;
  NINJ[10]=112080;
  NINJ[11]=112065;
  NINJ[12]=112112;
  NINJ[13]=112066;
  NINJ[14]=112085;
  NINJ[15]=112081;
  NINJ[16]=112080;
  NINJ[17]=112056;
  NINJ[18]=112047;
  //NINJ[19]=245410;

  //surveyed spacetime volume: 34.4067 Gpc^3 yr
  float SFmasses[NFACTOR][2] =
  {
    {60.0, 60.0},
    {60.0, 60.0},
    {100.0, 20.0},
    {100.0, 50.0},
    {100.0, 100.0},
    {100.0, 100.0},
    {100.0, 100.0},
    {200.0, 20.0},
    {210.0, 30.0},
    {200.0, 50.0},
    {200.0, 100.0},
    {300.0, 50.0},
    {200.0, 200.0},
    {300.0, 100.0},
    {400.0, 40.0},
    {420.0, 60.0},
    {300.0, 200.0},
    {300.0, 300.0},
    {400.0, 400.0},
    //{400.0, 400.0},
  };

  VT = 75.9783;
  TMC = 24537601.0;

  // read far vs rho background files 
  TString far_bin1_cut_file[1024];
  vector<TString> xfile = CWB::Toolbox::readFileList("config/far_rho.lst");
  for(int n=0;n<xfile.size();n++) far_bin1_cut_file[n+1]=xfile[n];
  if(xfile.size()!=nCHUNKS) {
     cout << endl;
     for(int n=0;n<xfile.size();n++) cout << n+1 << " " << far_bin1_cut_file[n+1] << endl;
     cout << endl << "user_pparameters.C error: config/far_rho.lst files' list size must be = "
          << nCHUNKS << endl << endl;
     exit(1);
  }

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out      = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot
  T_ifar     = 2.94;
  T_win      = 0.2;

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20;

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[2] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O1/DATA/%s/HVETO/L1/HVETO_L1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O1/DATA/%s/HVETO/H1/HVETO_H1_ALLO1_MERGED.txt",cwb_config_env,calibVer.Data());

}
