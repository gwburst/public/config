
#include "CWB_Plugin.h"

void CWB_PluginConfig() {

  // Config Plugin to generate injected 'on the fly' NR signals

  cout << "Execute CWB_PluginConfig.C ..." << endl;

  network** net;
  CWB_PLUGIN_IMPORT(network**,net);

  int* gIFACTOR;
  CWB_PLUGIN_IMPORT(int*,gIFACTOR);

  CWB::mdc* MDC;
  CWB_PLUGIN_IMPORT(CWB::mdc*,MDC);

  CWB::config** cfg;
  CWB_PLUGIN_IMPORT(CWB::config**,cfg);


  //All numbers, files etc. related to the the MonteCarlo....
  TString XML[19];

  XML[0]="U_60_60_nospin_modes_up-to_l4_noGT_injections_v5.xml.gz";
  XML[1]="U_60_60_aligned_modes_up-to_l4_injections_v5.xml.gz";
  XML[2]="U_100_20_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[3]="U_100_50_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[4]="U_100_100_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[5]="U_100_100_aligned_modes_up-to_l4_injections_v5.xml.gz";
  XML[6]="U_100_100_antialigned_modes_up-to_l4_injections_v5.xml.gz";
  XML[7]="U_200_20_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[8]="U_210_30_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[9]="U_200_50_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[10]="U_200_100_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[11]="U_300_50_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[12]="U_200_200_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[13]="U_300_100_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[14]="U_400_40_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[15]="U_420_60_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[16]="U_300_200_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[17]="U_300_300_nospin_modes_up-to_l4_injections_v5.xml.gz";
  XML[18]="U_400_400_nospin_modes_up-to_l4_injections_v5.xml.gz";
  //XML[19]="U_400_400_aligned_modes_up-to_l4_injections_v5.xml.gz";


  char st[512];
  sprintf(st,"--xml /home/gayathri.v/git_gayathri/imbhb/searches/injections/O1/NR_hdf5/%s",XML[(*gIFACTOR)-1].Data());
  TString inspOptions="";
  inspOptions+=st;
  inspOptions+= " --dir "+TString((*cfg)->tmp_dir)+" ";
  // the first parameter a the name of MDC defined by the user
  MDC->SetInspiral("NR_hdf5",inspOptions);
 
}

