
// -------------------------------------------------------------------------

// Definitions of the search bins used for Offline IMBH Search
// -------------------------------------------------------------------------

// definition of the selection selection cuts 
TCut norm_cut("norm_cut","norm>2.5");
TCut dqveto_cut("dqveto_cut","!veto_hveto_H1 && !veto_hveto_L1");
TCut fcut("fcut","frequency[0]>24 && frequency[0]<=256");
TCut chi2_cut("chi2_cut","log10(penalty)<0.3");
TCut netcc_cut("netcc_cut","netcc[0]>0.8 && netcc[2]>0.7");
TCut netccO1_cut("netccO1_cut","netcc[0]>0.8 && netcc[2]>0.8");
TCut qveto_cut("qveto_cut","Qveto[0]>(frequency[0]<90?0.1:0.3) ");
TCut chirp_cut("chirp_cut","abs(chirp[1])>10 && chirp[1]>-100");
TCut chi4_cut("chi4_cut","log10(penalty)<0.4");
TCut norm4_cut("norm4_cut","norm>4");


// definition of the inclusive bins
TCut bin1_cut_v1 = TCut("bin1_cut_v1",(norm_cut+dqveto_cut+fcut+chi2_cut+netccO1_cut+qveto_cut+chirp_cut).GetTitle());
TCut bin1_cut= TCut("bin1_cut",(norm4_cut+dqveto_cut+fcut+chi4_cut+netcc_cut+qveto_cut+chirp_cut).GetTitle());
