# Merged Plugins

The following plugins have been produced merging plugins from library/tools/cwb/plugins

* CWB_Plugin_Gating_QLveto.C  			(used for background productions)

  - CWB_Plugin_Gating.C
  - CWB_Plugin_QLveto.C
 
* CWB_Plugin_MDC_Frames_Gating_QLveto.C 	(used for simulations with frame injections) 

  - CWB_Plugin_MDC_Frames.C
  - CWB_Plugin_Gating.C
  - CWB_Plugin_QLveto.C

* CWB_Plugin_MDC_OTF_Gating_QLveto.C 		(used for simulations with builtin injections)

  - CWB_Plugin_MDC_OTF.C
  - CWB_Plugin_Gating.C
  - CWB_Plugin_QLveto.C

