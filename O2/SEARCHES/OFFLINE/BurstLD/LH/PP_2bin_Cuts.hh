// -------------------------------------------------------------------------------
// Definitions of the search bins used for the Offline Long Duration Search
// -------------------------------------------------------------------------------

// definition of the selection selection cuts
TCut dqveto_cut("dqveto_cut","!veto_hveto_H1 && !veto_hveto_L1");
TCut freq1_cut("freq1_cut","frequency[0]>24 && frequency[0]<2048");
TCut freq2_cut("freq2_cut","!(abs(frequency[0]-1011)<1)");
TCut freq3_cut("freq3_cut","(frequency[0]<200)");
TCut netcc_cut("netcc_cut","netcc[0]>0.6 && netcc[2]>0.6");
TCut dur_cut("dur_cut","duration[0]>1.5");

// definition of the exclusive bins
TCut bin1_cut    = TCut("bin1_cut",(dqveto_cut+freq1_cut+netcc_cut+dur_cut).GetTitle());
TCut bin2_cut    = TCut("bin2_cut",(dqveto_cut+freq1_cut+freq2_cut+netcc_cut+dur_cut).GetTitle());

TCut bin1FR_cut    = TCut("bin1FR_cut",(dqveto_cut+freq1_cut+freq3_cut+netcc_cut+dur_cut).GetTitle());
TCut bin2FR_cut    = TCut("bin2FR_cut",(dqveto_cut+freq1_cut+!freq3_cut+netcc_cut+dur_cut).GetTitle());

//cout << "bin1_cut : " << bin1_cut.GetTitle() << endl;
