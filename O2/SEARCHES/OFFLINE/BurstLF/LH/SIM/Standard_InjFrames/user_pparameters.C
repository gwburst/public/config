#define RUN_LABEL "BurstLF O2 #CWB_CALIB_VER K#CWB_CHUNK_NUMBER (Standard_InjFrames)"
//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO

#define FAR_BIN1_FILE_NAME "report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i0rho0_freq16_1024/data/far_rho.txt"
#define FAR_BIN2_FILE_NAME "report/postprod/M1.V_hvetoLH.C_bin2_cut.R_rMRA_hveto_i0cc00_i0rho0_freq16_1024/data/far_rho.txt"

{
  #include "O2/SEARCHES/OFFLINE/BurstLF/LH/PP_Cuts.hh"
  #include "O2/CHUNKS/Chunks_Cuts.hh"

  calibVer      = "#CWB_CALIB_VER";

  // Definitions of far_rho.txt files
  // must be modify according the background paths

  TString far_bin1_cut_file[100];
  TString far_bin2_cut_file[100];

  char xfile[1024];
  for(int n=2;n<=22;n++) {
    sprintf(xfile,"/home/vedovato/O2/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O2_K%02d_C00_LH_BurstLF_BKG_dbg3/%s", n, FAR_BIN1_FILE_NAME);         
    far_bin1_cut_file[n] = xfile;
    sprintf(xfile,"/home/vedovato/O2/OFFLINE/TESTS/TEST_BurstLF_CHUNKS/O2_K%02d_C00_LH_BurstLF_BKG_dbg3/%s", n, FAR_BIN2_FILE_NAME);         
    far_bin2_cut_file[n] = xfile;
  }

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot

  T_ifar     = 4.;        // ifar cut yrs
  //T_ifar     = 100.;        // ifar cut yrs

  pp_irho    = 0;
  pp_inetcc  = 0;
  pp_rho_min = 5.0;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;


  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O2/DATA/%s/HVETO/L1/HVETO_L1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O2/DATA/%s/HVETO/H1/HVETO_H1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());

}
