{                                                                                                    

cout << "Execute CWB_Plugin_MDC_OTF_Config.C ..." << endl;

  CWB::mdc MDC(net);

//All numbers, files etc. related to the the MonteCarlo....
  int XML[21][3] =
{
  {2, 1164556817, 1166486417},
  {3, 1167523218, 1169107218},
  {4, 1169107218, 1170174018},
  {5, 1170174018, 1170948618},
  {6, 1170948618, 1171632618},
  {7, 1171632618, 1172334618},
  {8, 1172334618, 1173188118},
  {9, 1173188118, 1173902418},
  {10, 1173902418, 1174651218},
  {11, 1174651218, 1175356818},
  {12, 1175356818, 1176240318},
  {13, 1176240318, 1176955218},
  {14, 1176955218, 1178294418},
  {15, 1179813618, 1181845818},
  {16, 1181845818, 1182825018},
  {17, 1182825018, 1184112018},
  {18, 1184112018, 1185217218},
  {19, 1185217218, 1185937218},
  {20, 1185937218, 1186624818},
  {21, 1186624818, 1187312718},
  {22, 1187312718, 1187740818}
};

//  int xstart = 1164556917;
  cout << "Start: " << xstart << endl;
//  int xstop  = 1164557817; 
  cout << "Stop: " << xstop << endl;
  
  int k = 0;
 // bool included = 0;
  while((xstart>XML[k][1]) && (k<22)) {
        //cout << "k=" << k << endl;
	if(xstop<XML[k][2]){cout << "Segment is fully included in chunk " << XML[k][0] << " " << XML[k][1] << " " << XML[k][2] << endl; break;}
        else {
	      if (xstart<XML[k][2]){cout << "Segment is not fully included in chunk " << XML[k][0] << " " << XML[k][1] << " " << XML[k][2] << endl;}	
              else{k++;}
        }
	//cout << "k=" << k << endl;	    
  }

  char st[1024];
  //sprintf(st,"-}, %s/O2/SEARCHES/OFFLINE/BBH/LH/SIM/BBH_BROAD_ISOTROPIC/XML/bbh_broad_isotropic},", cwb_config_env);
  TString IDIR = "/work/salemi/git/lvc-rates-and-pop/share/O2/injection_files"; 
  sprintf(st,"--xml %s/O2_%d/bbh_broad_isotropic-%d-%d.xml", IDIR.Data(), XML[k][0], XML[k][1], XML[k][2]);
  cout << "Opening XML file: " << st << endl;
  TString inspOptions="";
  inspOptions+=st;
  inspOptions+= " --dir "+TString(cfg->tmp_dir)+" ";

  // the first parameter a the name of MDC defined by the user
  MDC.SetInspiral("SEOBNRv3_opt_rk4pseudoFourPN",inspOptions);
 
}

