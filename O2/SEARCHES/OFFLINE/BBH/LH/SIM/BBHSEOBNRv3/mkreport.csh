#!/bin/tcsh -f

onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif

${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K02_cut --mcuts time[0]>1164556817&&time[0]<=1166486417 --label O2_K02_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K03_cut --mcuts time[0]>1166486417&&time[0]<=1169107218 --label O2_K03_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K04_cut --mcuts time[0]>1169107218&&time[0]<=1170174018 --label O2_K04_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K05_cut --mcuts time[0]>1170174018&&time[0]<=1170948618 --label O2_K05_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K06_cut --mcuts time[0]>1170948618&&time[0]<=1171632618 --label O2_K06_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K07_cut --mcuts time[0]>1171632618&&time[0]<=1172334618 --label O2_K07_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K08_cut --mcuts time[0]>1172334618&&time[0]<=1173188118 --label O2_K08_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K09_cut --mcuts time[0]>1173188118&&time[0]<=1173902418 --label O2_K09_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K10_cut --mcuts time[0]>1173902418&&time[0]<=1174649000 --label O2_K10_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K11_cut --mcuts time[0]>1174649000&&time[0]<=1175356818 --label O2_K11_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K12_cut --mcuts time[0]>1175356818&&time[0]<=1176240318 --label O2_K12_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K13_cut --mcuts time[0]>1176240318&&time[0]<=1176955218 --label O2_K13_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K14_cut --mcuts time[0]>1176955218&&time[0]<=1178294418 --label O2_K14_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K15_cut --mcuts time[0]>1178294418&&time[0]<=1181845818 --label O2_K15_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K16_cut --mcuts time[0]>1181845818&&time[0]<=1182825018 --label O2_K16_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K17_cut --mcuts time[0]>1182825018&&time[0]<=1184112018 --label O2_K17_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K18_cut --mcuts time[0]>1184112018&&time[0]<=1185217218 --label O2_K18_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K19_cut --mcuts time[0]>1185217218&&time[0]<=1185937218 --label O2_K19_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K20_cut --mcuts time[0]>1185937218&&time[0]<=1186624818 --label O2_K20_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K21_cut --mcuts time[0]>1186624818&&time[0]<=1187312718 --label O2_K21_cut'
${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL '--tcuts O2_K22_cut --mcuts time[0]>1187312718&&time[0]<=1187740818 --label O2_K22_cut'

$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K02_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K03_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K04_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K05_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K06_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K07_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K08_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K09_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K10_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K11_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K12_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K13_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K14_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K15_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K16_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K17_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K18_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K19_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K20_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K21_cut
$HOME_WAT/tools/cwb/postproduction/scripts/cbc_plots_ifar.csh $CWB_MERGE_LABEL.C_O2_K22_cut

unsetenv CWB_MERGE_LABEL

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1


