#include "TF1.h"
#include "Math/WrappedTF1.h"
#include "Math/GaussIntegrator"

static const double G = 6.67384e-11;
static const double SM = 1.9891e30;
static const double C = 299792458;
static const double TimeUnit = G*SM/C/C/C;

#define MSMBH 3.5e6
#define MMIN  5.
#define MMAX  50.
#define R_FID 3000000 // 3 Gpc

#define FREQ_CUTOFF 24  // Hz
#define NEVENTS 15000

#define FACTORS 140

int CreateListEBBH()
{
  double mSMBH = MSMBH;
  double mmin  = MMIN;
  double mmax  = MMAX;
  double beta  = 2;
  double R_fid = R_FID;
  GNGen eGen(mSMBH, mmin, mmax, beta);
  eGen.setFreqCutoff(FREQ_CUTOFF);

  double binevents[15][15];
  for(int qq=0; qq<15; qq++)
     for(int rr=0; rr<15; rr++)
         binevents[qq][rr]=0;

  char Fx[] = "pow(x, 29./19)*pow(1.+121./304*x*x, 1181./2299)/pow(1.-x*x,3./1)";
  TF1 ff("Eccentricity Decay", Fx, 0., TMath::Pi());
  
  ROOT::Math::WrappedTF1 wf1(ff);
  ROOT::Math::GaussIntegrator ig;

  ig.SetFunction(wf1);
  ig.SetRelTolerance(0.0001);

  double m1, m2, rp, e, r_fid, r_num, inj_chirp_mass;
  char fName[256];
  TRandom3 rnd;
  rnd.SetSeed(0);

  for(int i=0; i<=FACTORS; i++) {
     for(int qq=0; qq<15; qq++)
        for(int rr=0; rr<15; rr++)
            binevents[qq][rr]=0;
      FILE* f = stdout; int exc_ev=0;
      sprintf(fName,"eBBH_%d.lst",i);
      f=fopen(fName, "w");
      int nevents=0;
      while(nevents<NEVENTS) {
            {
             eGen.generateEvent(m1, m2, rp, e);

             double M = m1+m2;
             double eta = (m1*m2)/(M*M);
             double tmerger = (15./19)*pow(304./425, 3480./2299)*(M/eta)*pow(rp, 4.)*ig.Integral(0., e)*TimeUnit;

             inj_chirp_mass = pow(m1*m2, 3./5.)/pow(m1+m2, 1./5.);
             r_fid = R_fid*pow(inj_chirp_mass/43.52,5./6.); // fiducial radius for the generated event scaled based on chirp mass
             r_num = rnd.Rndm();
             r_fid *= sqrt(r_num); // generating event radius between 0 and scaled fiducial radius with r^2 distribution
             if((rp<16 && tmerger<2000.) || tmerger<120.) {
                       //int qq = (m1-5)/4; int rr = (m2-5)/4;
                       //if(qq==5) qq--; if(rr==5) rr--;
                       //if(binevents[rr][qq]>NEVENTS/15+10) continue; else binevents[rr][qq]++;
                       printf("%d %lf %lf %lf %.8lf %lf %lf\n", nevents, m1, m2, rp, e, r_fid, tmerger);
                       fprintf(f,"%d %lf %lf %lf %.8lf %lf\n", nevents++, m1, m2, rp, e, r_fid);
              } else exc_ev++;
            }
          }
  fclose(f);
  cout<<"--------------------------------------------------------------"<<endl;
} cout<<exc_ev<<endl;
  gSystem->Exit(0);
}
