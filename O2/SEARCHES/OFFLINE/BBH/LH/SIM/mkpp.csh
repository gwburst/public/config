#!/bin/tcsh -f

set called=($_)

if ( "$called" != "" ) then  ### called by source
   echo "branch 1"
   set script_fn=`readlink -f $called[2]`
else                         ### called by direct excution of the script
   echo "branch 2"
   set script_fn=`readlink -f $0`
endif

echo "A:$0"
echo "B:$called"
set script_dir=`dirname $script_fn`

echo "script file name=$script_fn"
echo "script dir=$script_dir"


onintr irq_ctrlc

unsetenv CWB_MERGE_LABEL
unsetenv CWB_CHUNK_START
unsetenv CWB_CHUNK_STOP
unsetenv CWB_PP_OPTIONS
unsetenv CWB_CHUNK_SEARCH

if ((( $1 == '' ))) then
  echo ""
  echo "CWB_MERGE_LABEL not defined"
  echo ""
  exit 1
else
  setenv CWB_MERGE_LABEL $1
endif

if ((( $2 == '' ))) then
  echo ""
  echo "CWB_CHUNK_START not defined"
  echo ""
  exit 1
else
  setenv CWB_CHUNK_START $2
endif

if ((( $3 == '' ))) then
  echo ""
  echo "CWB_CHUNK_STOP not defined"
  echo ""
  exit 1
else
  setenv CWB_CHUNK_STOP $3
endif

if ((( $4 != 'merge' ) && ( $4 != 'veto' ) && ( $4 != 'cut' ) && ( $4 != 'ifar' ) && ( $4 != 'pe' ) && ( $4 != 'report' ) && ( $4 != 'all' ) &&( $4 != '' ))) then
  echo ""
  echo \'$4\' "is a wrong cwb pp option"
  echo ""
  echo "available options"
  echo "          no options -> all (default)"
  echo "          merge : execute merge"
  echo "          veto  : execute veto"
  echo "          cut   : execute cut"
  echo "          ifar  : execute ifar"
  echo "          pe    : execute pe"
  echo "          report: execute report"
  echo "          all   : execute merge+veto+cut+ifar+pe+report"
  echo ""
  exit 1
else
  if ((( $4 == '' ))) then
    setenv CWB_PP_OPTIONS  "all"
  else
    setenv CWB_PP_OPTIONS  $4
  endif
endif

if ((( $5 == '' ))) then
  setenv CWB_CHUNK_SEARCH "BBH"
else
  setenv CWB_CHUNK_SEARCH $5
endif

if ((( $CWB_PP_OPTIONS == "merge" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_merge.csh   $CWB_MERGE_LABEL '--nthreads 8'
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "veto" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_setveto.csh $CWB_MERGE_LABEL
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "cut" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_setcuts.csh $CWB_MERGE_LABEL.V_hvetoLH '--unique true'
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "ifar" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  @ K = $CWB_CHUNK_START
  while ($K <= $CWB_CHUNK_STOP)
    set KK=`echo $K | awk '{ printf "%02d\n", $0 }'`
    if($K == $CWB_CHUNK_START) then
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U '           --tsel bin1_cut&&O2_K'$KK'_cut  --label bin1_cut --file far_bin1_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    else 
      ${CWB_SCRIPTS}/cwb_setifar.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1_cut '--tsel bin1_cut&&O2_K'$KK'_cut  --label same     --file far_bin1_cut_file['$KK'] --mode exclusive'
      if ( $? != 0) exit 1
    endif
    @ K += 1
  end
endif

if ((( $CWB_PP_OPTIONS == "pe" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_report.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1_cut pe
  if ( $? != 0) exit 1
endif

if ((( $CWB_PP_OPTIONS == "report" ) || ( $CWB_PP_OPTIONS == "all" ))) then
  ${CWB_SCRIPTS}/cwb_report_cbc.csh $CWB_MERGE_LABEL.V_hvetoLH.C_U.S_bin1_cut $CWB_CHUNK_SEARCH
  if ( $? != 0) exit 1
endif

unsetenv CWB_MERGE_LABEL
unsetenv CWB_CHUNK_START
unsetenv CWB_CHUNK_STOP
unsetenv CWB_PP_OPTIONS
unsetenv CWB_CHUNK_SEARCH

exit 0
irq_ctrlc:
  ps T | grep root | awk '{print $1}' | xargs kill -9
  exit 1

