
#define nRHO 300

// -----------------------------------------------------------
// CBC
// -----------------------------------------------------------

#define RUN_LABEL "O2 IMBH redshifted SEOBNRv4 12 MDC sets: C02 Callbration"

#define MIN_plot_mass1 0.0
#define MAX_plot_mass1 400
#define MIN_plot_mass2 0.0
#define MAX_plot_mass2 400

#define MAX_EFFECTIVE_RADIUS  5000.
#define MASS_BIN 30.0
#define MIN_MASS 0.0
#define MAX_MASS 630.

#define MINCHI -1
#define MAXCHI 1
#define CHI_BIN 0.15

#define TOLERANCE 0.01

#define BKG_NTUPLE "True"
//#define OPEN_LAG 100
#define PLOT_CHIRP
#define PLOT_MASS_RATIO
//#define SKIP_GETLIVETIME
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
#define WRITE_ASCII
//#define SKIP_GETLIVETIME
//#define LIVE_ZERO 3978522.0
#define LIVE_ZERO 442056.0  //K19


#define PLOT_CHIRP
#define PLOT_MASS_RATIO
//#define SKIP_GETLIVETIME
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
//#define FIXMAXDISTANCE 2600000
#define FIXMAXDISTANCE 6800000
#define FIXMINDISTANCE 0


#define FIXMINRATIO 0.95
#define FIXMAXRATIO 10.05
//#define FIXMINTOT 298
//#define FIXMAXTOT 302
#define FIXMINTOT MIN_plot_mass1+MIN_plot_mass2
#define FIXMAXTOT MAX_plot_mass1+MAX_plot_mass2
#define WRITE_ASCII
#define REDSHIFT

//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO

#define FAR_BIN1_FILE_NAME "report/postprod/M1.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt"

{

  #include <O2/SEARCHES/OFFLINE/IMBHB/LH/PP_Cuts.hh>
  #include <O2/CHUNKS/Chunks_Cuts.hh>

  #define nCHUNKS       21
  #define NFACTOR       12
  // ---------------------------------------------------------------
  // All numbers, files etc. related to the the MonteCarlo....
  // ---------------------------------------------------------------

  user_pp_label = "";
  //INCLUDE_INTERNAL_VOLUME = 0;
  //TRIALS = 1;


  int NINJ[NFACTOR];
  NINJ[0]=245403;
  NINJ[1]=245393;
  NINJ[2]=245438;
  NINJ[3]=245334;
  NINJ[4]=245329;
  NINJ[5]=245427;
  NINJ[6]=245409;
  NINJ[7]=245360;
  NINJ[8]=245376;
  NINJ[9]=245411;
  NINJ[10]=245367;
  NINJ[11]=245380;

  float SFmasses[NFACTOR][2];
  SFmasses[0][0]=100.0;
  SFmasses[0][1]=100.0;
  SFmasses[1][0]=100.0;
  SFmasses[1][1]=100.0;
  SFmasses[2][0]=100.0;
  SFmasses[2][1]=100.0;
  SFmasses[3][0]=100.0;
  SFmasses[3][1]=20.0;
  SFmasses[4][0]=100.0;
  SFmasses[4][1]=50.0;
  SFmasses[5][0]=200.0;
  SFmasses[5][1]=100.0;
  SFmasses[6][0]=200.0;
  SFmasses[6][1]=200.0;
  SFmasses[7][0]=200.0;
  SFmasses[7][1]=50.0;
  SFmasses[8][0]=300.0;
  SFmasses[8][1]=100.0;
  SFmasses[9][0]=300.0;
  SFmasses[9][1]=200.0;
  SFmasses[10][0]=300.0;
  SFmasses[10][1]=300.0;
  SFmasses[11][0]=300.0;
  SFmasses[11][1]=50.0;

  //surveyed spacetime volume: 34.4067 Gpc^3 yr
  VT = 75.9783;
  TMC = 24537601;


  // read far vs rho background files 
  TString far_bin1_cut_file[1024];
  vector<TString> xfile = CWB::Toolbox::readFileList("config/far_rho.lst");
  for(int n=0;n<xfile.size();n++) far_bin1_cut_file[n+2]=xfile[n];
  if(xfile.size()!=nCHUNKS) {
     cout << endl;
     for(int n=0;n<xfile.size();n++) cout << n+1 << " " << far_bin1_cut_file[n+2] << endl;
     cout << endl << "user_pparameters.C error: config/far_rho.lst files' list size must be = "
          << nCHUNKS << endl << endl;
     exit(1);
  }


  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out      = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot
  T_ifar     = 2.94;
  T_win      = 0.2;

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20;

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[2] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O2/DATA/%s/HVETO/L1/HVETO_L1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O2/DATA/%s/HVETO/H1/HVETO_H1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());

}

