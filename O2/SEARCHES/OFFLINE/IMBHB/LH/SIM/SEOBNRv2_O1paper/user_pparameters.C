
#define nRHO 300

// -----------------------------------------------------------
// CBC
// -----------------------------------------------------------

#define MIN_plot_mass1 0.0
#define MAX_plot_mass1 400
#define MIN_plot_mass2 0.0
#define MAX_plot_mass2 400

#define MAX_EFFECTIVE_RADIUS  5000.
#define MASS_BIN 30.0
#define MIN_MASS 0.0
#define MAX_MASS 630.

#define MINCHI -1
#define MAXCHI 1
#define CHI_BIN 0.15

#define TOLERANCE 0.01

#define RUN_LABEL "O2 IMBH redshifted SEOBNRv2 12 MDC sets: C02 Callbration"

#define BKG_NTUPLE "True"
//#define OPEN_LAG 100
#define PLOT_CHIRP
#define PLOT_MASS_RATIO
//#define SKIP_GETLIVETIME
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
#define WRITE_ASCII
//#define SKIP_GETLIVETIME
#define LIVE_ZERO 3978522.0

#define PLOT_CHIRP
#define PLOT_MASS_RATIO
//#define SKIP_GETLIVETIME
#define VOL_Mtotq  ///Volume calculated not correcting to make it uniform in M1 and M2
//#define FIXMAXDISTANCE 2600000
#define FIXMAXDISTANCE 6800000
#define FIXMINDISTANCE 0


#define FIXMINRATIO 0.95
#define FIXMAXRATIO 10.05
//#define FIXMINTOT 298
//#define FIXMAXTOT 302
#define FIXMINTOT MIN_plot_mass1+MIN_plot_mass2
#define FIXMAXTOT MAX_plot_mass1+MAX_plot_mass2
#define WRITE_ASCII
#define REDSHIFT

//#define RUN_LABEL "IMBHB O2 #CWB_CHUNK_CAL (SEOBNRv2_O1paper)"
//#define CAT2_VETO
//#define CAT3_VETO
#define HVETO_VETO
{
  #include "O2/SEARCHES/OFFLINE/IMBHB/LH/PP_Cuts.hh"
  #include "O2/CHUNKS/Chunks_Cuts.hh"

  calibVer      = "#CWB_CALIB_VER";
  
  user_pp_label = "";
  bool INCLUDE_INTERNAL_VOLUME = 0;
  int TRIALS = 1;
  TString far_bin1_cut_file[100];
  char xfile[1024];

  for(int n=1;n<=9;n++) {
    sprintf(xfile,"/home/vedovato/O1/REPROCESSING/IMBHB/O1_K%02d_C02_LH_IMBHB_BKG_run1/report/postprod/M2.V_hvetoLH.C_bin1_cut.R_rMRA_hveto_i0cc00_i1rho0_freq16_1024/data/far_rho.txt",n);
    far_bin1_cut_file[n] = xfile;
  }

  T_cor      = 0.0;       // cc cut
  T_cut      = 0.0;       // rho high frequency cut
  T_out	     = 0.0;
  hours      = 1.;        // bin size in hours for rate vs time plot
  T_ifar     = 4.00812428565954726e-01;
  T_win      = 0.2;

  pp_irho    = 1;
  pp_inetcc  = 0;
  pp_rho_min = 4.5;
  pp_rho_max = 10;
  pp_drho    = 0.1;

  pp_max_nloudest_list = 20; 

//  pp_jet_benckmark = -1;
//  pp_mem_benckmark = -1;

  // ----------------------------------------------------------
  // VETO cuts
  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  // ----------------------------------------------------------
  const int nvdqf=2;
  dqfile vdqf[nvdqf] = {
         {"L1" ,"",     CWB_HVETO, 0., false,   false},
         {"H1" ,"",     CWB_HVETO, 0., false,   false}
  };

  sprintf(vdqf[0].file, "%s/O2/DATA/%s/HVETO/L1/HVETO_L1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
  sprintf(vdqf[1].file, "%s/O2/DATA/%s/HVETO/H1/HVETO_H1_ANALYSIS_SEGMENTS_MERGED.txt",cwb_config_env,calibVer.Data());
 
}
