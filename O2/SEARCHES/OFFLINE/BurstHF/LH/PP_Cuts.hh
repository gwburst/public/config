// -------------------------------------------------------------------------------
// Definitions of the search bins used for the Offline High Frequency Burst Search
// -------------------------------------------------------------------------------

// definition of the selection selection cuts 
//TCut dqveto_cut("dqveto_cut","veto_cat2_H1 && veto_cat2_L1 && !veto_cat3_H1 && !veto_cat3_L1 && !veto_hveto_H1 && !veto_hveto_L1");
TCut dqveto_cut("dqveto_cut","!veto_hveto_H1 && !veto_hveto_L1");
TCut norm_cut("norm_cut","norm>2.5");
TCut fcut0_cut("fcut0_cut","frequency[0]>896 && frequency[0]<=4096");
TCut fcut1_cut("fcut1_cut","(frequency[0]>=1000 && frequency[0]<=1150) || (frequency[0]>=2000 && frequency[0]<=2500)");
TCut fcut2_cut("fcut2_cut","(frequency[0]>896 && frequency[0]<1000) || (frequency[0]>1150 && frequency[0]<2000) || (frequency[0]>2500 && frequency[0]<=4096)");
TCut netcc_cut("netcc_cut","netcc[0]>0.8 && netcc[2]>0.8");

// definition of the exclusive bins
TCut bin1_cut    = TCut("bin1_cut",(dqveto_cut+norm_cut+fcut0_cut+netcc_cut).GetTitle());
//TCut bin1_cut    = TCut("bin1_cut",(dqveto_cut+norm_cut+fcut1_cut+netcc_cut).GetTitle());
//TCut bin1_cut    = TCut("bin1_cut",(dqveto_cut+norm_cut+fcut2_cut+netcc_cut).GetTitle());

//cout << "bin1_cut : " << bin1_cut.GetTitle() << endl;
