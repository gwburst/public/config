# O2 Configuration Structure #

## setup of O2 environment ##
* setup.csh

## files with chunk periods ##
* CHUNKS

## dq & frame lists ##
* DATA
	* C00  
		* DQ (data quality cat0/1/2/4)
			* BURST(dq burst)
			* BURST_HF(dq burst hf)
			* BURST_IMBH(dq burst_imbh)

		* FRAMES (frame file lists)
			* ATLAS (atlas frame file lists)
			* CIT (cit frame file lists)

		* HVETO 
	* C01  
		* same structure as C00
	* C02  
		* same structure as C00
	* C02c   
		* DQ (symbolic link to C02/DQ)
		* Cleaned FRAMES (Review page : https://wiki.ligo.org/DAC/NoiseSubtractionReview)

	* V1Online  
		* DQ (data quality cat0/1 Virgo)
			* FLORENT

		* FRAMES (frame file lists)
			* ATLAS (atlas frame file lists)
			* CIT (cit frame file lists)
	
## macros used to generate configuration files ##
* MACROS

## scripts used to manage chunk BKG/SIM productions ##
* SCRIPTS
	* cwb_mkchunk.csh 
	* cwb_ppchunk.csh 
	* cwb_clchunk.csh 
	* cwb_lschunk.csh 

## files which contain the list of slags used in the search analysis & slagSizeMax.lst (max slagSize for eaxh chunk)##
* SLAGS
	* LH (LH network)
		* C00  	
		* C01  
		* C02  
		* C02c	(symbolic link to C02)  

## cwb searches ##
* SEARCHES
	* PLUGINS (cwb plugins used in the analysis)
	* OFFLINE
		* BurstLF (burst allsky low frequency)
			- LH
				* PP_Cuts.hh  (PP cuts file used for BurstLF)
				* BKG (background configuration files)
				  - mkpp.csh (post-production script used for BKG BurstLF)
				* SIM
				  - mkpp.csh (post-production script used for SIM BurstLF)
				  - Standard_InjBuiltin (Standard MDC set with built-in injections)
				  - Standard_InjFrames  (Standard MDC set with injections from frame files)
		* BurstHF (burst allsky high frequency)
			- LH
				* PP_Cuts.hh  (PP cuts file used for BurstHF)
				* BKG (background configuration files)
				  - mkpp.csh (post-production script used for BKG BurstHF)
				* SIM
				  - mkpp.csh (post-production script used for SIM BurstLF)
				  - Standard_InjBuiltin (Standard MDC set with built-in injections)
				  - Standard_InjFrames  (Standard MDC set with injections from frame files)
		* BurstLD (burst allsky long duration)
			- LH
				* PP_Cuts.hh  (PP cuts file used for BurstLD)
				* BKG (background configuration files)

		* BBH (allsky stellar mass bbh)
			- LH
				* PP_Cuts.hh  (PP cuts file used for BBH)
				* BKG (background configuration files)
		* IMBHB (allsky imbhb)
			- LH
				* PP_Cuts.hh  (PP cuts file used for IMBHB)
				* SIM

	* ONLINE (low latency searches)
		* BurstLF
			- LH
				* ZL	(Zero Lag)
				* BKG	(Background)
			- LHV
		* BBH
			- LH
				* ZL	(Zero Lag)
				* BKG	(Background)
			- LHV


