/*
# Copyright (C) 2019 Gabriele Vedovato
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include "../../MACROS/ReadChunkList.C"

#define SEED			150914

#define CHUNK_FILE_LIST         "Chunk_List.txt"
#define CHUNK_MAX_SIZE          100

#define EXECUTE

void Create_SlagRndFile(TString slags_dir, int chunkID, int slagMax, char* cwb_config_env, char* calibVer, char* calibType);
void shuffle(std::vector<int>& vector, int seed=150914);

void Create_O2_LHV_ChunkSlagRndFiles(TString slags_dir, TString calibVer, TString calibType) {

  CWB::Toolbox TB;

  // get CWB_CONFIG
  char cwb_config_env[1024] = "";
  if(gSystem->Getenv("CWB_CONFIG")!=NULL) {
    strcpy(cwb_config_env,TString(gSystem->Getenv("CWB_CONFIG")).Data());
  }

  char chunk_file_list[1024];
  sprintf(chunk_file_list,"%s/O2/CHUNKS/%s",cwb_config_env,CHUNK_FILE_LIST);
  cout << chunk_file_list << endl;

  int    chunk[CHUNK_MAX_SIZE];
  double start[CHUNK_MAX_SIZE];
  double stop[CHUNK_MAX_SIZE];

  int nChunks = ReadChunkList(chunk_file_list,chunk,start,stop);


  int nIFO = 3;

  char ifo[NIFO_MAX][8];
  strcpy(ifo[0],"L1");
  strcpy(ifo[1],"H1");
  strcpy(ifo[2],"V1");

  vector<TString> ifos(nIFO);
  for(int n=0;n<nIFO;n++) ifos[n]=ifo[n];

  int slagMin    = 0;       // if slagMax=0 ->  slagMin must be < slagMax
  int slagOff    = 0;
  size_t* slagSite = NULL;  // site index starting with 0
  char*   slagFile = NULL;  // slag file list

  //jobs
//  int segLen  = 1200;
int segLen  = 1127;

  // dq file list
  // {ifo, dqcat_file, dqcat[0/1/2], shift[sec], inverse[false/true], 4columns[true/false]}
  int nDQF=13;
  dqfile DQF[13]={

                    {"L1" ,"",   CWB_CAT0, 0., false, false},
                    {"H1" ,"",   CWB_CAT0, 0., false, false},
                    {"V1" ,"",   CWB_CAT0, 0., false, false},

                    {"L1" ,"",   CWB_CAT1, 0., true,  false},
                    {"H1" ,"",   CWB_CAT1, 0., true,  false},
                    {"V1" ,"",   CWB_CAT1, 0., true,  false},

                    {"L1" ,"",   CWB_CAT2, 0., true,  false},
                    {"H1" ,"",   CWB_CAT2, 0., true,  false},

                    {"L1" ,"",   CWB_CAT1, 0., true,  false},
                    {"H1" ,"",   CWB_CAT1, 0., true,  false},

                    {"L1" ,"",   CWB_CAT0, 0., false, false},
                    {"H1" ,"",   CWB_CAT0, 0., false, false},
                    {"V1" ,"",   CWB_CAT0, 0., false, false}

                   };

  sprintf(DQF[0].file, "%s/O2/DATA/%s/DQ/%s/L1_cat0.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[1].file, "%s/O2/DATA/%s/DQ/%s/H1_cat0.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[2].file, "%s/O2/DATA/%s/DQ/%s/V1_cat0.txt",cwb_config_env,calibVer.Data(),calibType.Data());

  sprintf(DQF[3].file, "%s/O2/DATA/%s/DQ/%s/L1_cat1.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[4].file, "%s/O2/DATA/%s/DQ/%s/H1_cat1.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[5].file, "%s/O2/DATA/%s/DQ/%s/V1_cat1.txt",cwb_config_env,calibVer.Data(),calibType.Data());

  sprintf(DQF[6].file, "%s/O2/DATA/%s/DQ/%s/L1_cat2.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[7].file, "%s/O2/DATA/%s/DQ/%s/H1_cat2.txt",cwb_config_env,calibVer.Data(),calibType.Data());

  sprintf(DQF[8].file, "%s/O2/DATA/%s/DQ/%s/L1_cat4.txt",cwb_config_env,calibVer.Data(),calibType.Data());
  sprintf(DQF[9].file, "%s/O2/DATA/%s/DQ/%s/H1_cat4.txt",cwb_config_env,calibVer.Data(),calibType.Data());

#ifdef EXECUTE
  char ofile[1024];
  sprintf(ofile,"%s/%s/%s/slagSizeMax.lst",slags_dir.Data(),calibVer.Data(),calibType.Data());
  cout << ofile << endl;
  ofstream out;
  out.open(ofile);
  if (!out.good()) {cout << "Error Opening File : " << ofile << endl;exit(1);}
  out << "# chunk	slagSize" << endl << endl;
#endif
  cout << "# chunk	slagSize" << endl << endl;

  for(int k=17;k<nChunks;k++) {

    sprintf(DQF[10].file, "%s/O2/CHUNKS/K%02d.period",cwb_config_env,chunk[k]);
    sprintf(DQF[11].file, "%s/O2/CHUNKS/K%02d.period",cwb_config_env,chunk[k]);
    sprintf(DQF[12].file, "%s/O2/CHUNKS/K%02d.period",cwb_config_env,chunk[k]);

    vector<waveSegment> cat1List=TB.readSegList(nDQF, DQF, CWB_CAT1);

    int slagMax=TB.getSlagJobList(cat1List, segLen).size();
     
    char string[64]; sprintf(string,"K%02d\t%d",chunk[k],2*slagMax+1);
    cout << "slagSize = \t" << string << endl;

    Create_SlagRndFile(slags_dir, chunk[k], slagMax, cwb_config_env, (char*)calibVer.Data(), (char*)calibType.Data());

#ifdef EXECUTE
    out << string << endl;
#endif

  }

#ifdef EXECUTE
  out.close();
#endif

  exit(0);
}

void Create_SlagRndFile(TString slags_dir, int chunkID, int slagMax, char* cwb_config_env, char* calibVer, char* calibType) {

  std::vector<int> RND_A(2*slagMax);
  std::vector<int> RND_B(2*slagMax);
  for(int n=0; n<slagMax; n++) {
    RND_A[2*n+0]=+(n+1);
    RND_A[2*n+1]=-(n+1);

    RND_B[2*n+0]=-(n+1);
    RND_B[2*n+1]=+(n+1);
  }

  shuffle(RND_A,SEED);		// shuffle data RND_A
  shuffle(RND_B,SEED);		// shuffle data RND_B

  // check RND_A
  for(int n=0; n<slagMax; n++) {
    int A = +(n+1);
    int B = -(n+1);
    int nA=0;
    int nB=0;
    for(int n=0; n<2*slagMax; n++) {
      if(RND_A[n]==A) nA++;
      if(RND_A[n]==B) nB++;
    }
    if(nA!=1 && nB!=1) {
      cout << "ERROR !!!" << endl;
      exit(1);
    }
  }

  // check RND_B
  for(int n=0; n<slagMax; n++) {
    int A = +(n+1);
    int B = -(n+1);
    int nA=0;
    int nB=0;
    for(int n=0; n<2*slagMax; n++) {
      if(RND_B[n]==A) nA++;
      if(RND_B[n]==B) nB++;
    }
    if(nA!=1 && nB!=1) {
      cout << "ERROR !!!" << endl;
      exit(1);
    }
  }

  //for(int n=0; n<2*slagMax; n++) cout << n << "\t" << RND_A[n] << endl;;
  //for(int n=0; n<2*slagMax; n++) cout << n << "\t" << RND_B[n] << endl;;

  char ofile[1024];
  sprintf(ofile,"%s/%s/%s/K%02d.slags",slags_dir.Data(),calibVer,calibType,chunkID);
  cout << ofile << endl;

#ifdef EXECUTE
  ofstream out;
  out.open(ofile);
  if (!out.good()) {cout << "Error Opening File : " << ofile << endl;exit(1);}
  out << 0 << "\t" << 0 << "\t" << 0 << "\t" << 0 << endl;
  for(int n=0; n<2*slagMax; n++) {
    out << n+1 << "\t" << 0 << "\t" <<  RND_A[n] << "\t" <<  RND_B[n] << endl;
  }
  out.close();
#endif

  return;
}

void shuffle(std::vector<int>& vector, int seed) {

  TRandom3 rnd3(seed);

  size_t size = vector.size();

  if(size > 1) {
    size_t i;
    for(size_t i=0; i<size; i++) {
      size_t j = int(rnd3.Uniform(0,size));
      int t = vector[j];
      vector[j] = vector[i];
      vector[i] = t;
    }
  }
}
