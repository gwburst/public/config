# xTalk Catalog Files #

The xTalk catalog files contain the overlapping coefficients between WDM pixels and are used by wavelet-packet to reconstruct the signal using pixels at different resolution levels.

The overlap catalog files used for the Second Observational run O2 searches are:

## BurstLF, BurstHF, BBH, IMBHB ##

* **wdmXTalk/OverlapCatalog16-1024.bin**
  * is equivalent to **OverlapCatalog-ilLev4-hLev10-iNu4-P10.xbin**
  * The file is generated with the following command: **cwb_xtalk 4 10 4 10**

## BurstLD ##

* **wdmXTalk/OverlapCatalog-ilLev6-hLev12-iNu6-P10.xbin**
  * The file is generated with the following command: **cwb_xtalk 6 12 6 10**

## Development ##

The following overlap catalog:

* **wdmXTalk/OverlapCatalog-ilLev3-hLev9-iNu6-P10.xbin**
  * The file is generated with the following command: **cwb_xtalk 3 9 6 10**

can be used for analysis with data resampling at 1KHz   